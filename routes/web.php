<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Route::get('/check', function () {
	$coffee1 = new \App\CoffeeBuilder("Large Cup Of Coffee");
	$coffee1->add('chocolate');
	$coffee1->add('vanilla');
	$coffee1->add('vanilla');
	$coffee1->add('whole milk');
	$coffee1->remove('whole milk');
	$coffee1->add('skim milk');
	$price = $coffee1->getPrice(); // should return $5.35
	$addons = $coffee1->getAddOns(); // should return the addons used
	info($price);
	info($addons);
	$coffee2 = $coffee1->copy();
	$coffee2->remove('chocolate');

	$sale = app('coffee-service')->recordSale([$coffee1, $coffee2]);
	$price = $sale->price; // should return $9.85
	info($price);
	$sold_at = $sale->sold_at; // should return a Carbon object from the time of the sale.
	info($sold_at);
});