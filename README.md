#Koffee Company Code Challenge
------------------------------

This challenge is expected to take 4 hours. It aims to test your knowledge of model persistance, service class creation, object building, queues, events, listeners, and phpunit.

The Koffee Company is looking to build an app for creating and selling coffee drinks.  Using Laravel, MySql, and SqlLite, achieve the following.

##Given 

    1. Three persisted models should exist: one for sales, and the other for coffee products.
    2. The ingredients will include:  
        ◦ Base ingredient: Small Cup Of Coffee priced at $1.00
        ◦ Base ingredient: Large Cup Coffee priced at $2.00
        ◦ Add-on ingredient: 1 pump of chocolate priced at $0.85
        ◦ Add-on ingredient: 1 pump of vanilla priced at $0.75
        ◦ Add-on ingredient: 1 oz whole milk priced at $1.25
        ◦ Add-on ingredient: 1 oz skim milk priced at $1.00

##Creating 
An coffee building class should be build which allows the following pseudocode to function properly

$coffee1 = new CoffeeBuilder(Large Cup Of Coffee);
$coffee1->add(chocolate);
$coffee1->add(vanilla);
$coffee1->add(vanilla);
$coffee1->add(whole milk);
$coffee1->remove(whole milk);
$coffee1->add(skim milk);
$coffee1->getPrice(); // should return $5.35
$coffee1->getAddOns() // should return the addons used
$coffee2 = $coffee1->copy();
$coffee2->remove(chocolate);

##Persistence via Service class

$sale = app(‘coffee-service’)->recordSale([$coffee1,coffee2]) 
	$sale->price // should return $9.85
	$sale->sold_at // should return a Carbon object from the time of the sale.

** As soon as a sale is made, a “thank you” mail should be queued up and sent out (use log driver for mail)

##Seeding
php artisan migrate with seeding should seed the database with all the ingredients and 10 fake sales, each having a unique cup of coffee.

##Testing
The following tests should be performed - making sure that tests do not affect the production database and instead use sqllite.
    • ACoffeeCannotHaveMoreThan5Addons
    • ARandomCoffeeWithMultipleAddonsGeneratesTheCorrectPrice
    • ASingleCoffeeSaleGeneratesARecordInTheDatabase
    • ACoffeeCannotBeCreatedWithoutABaseIngredient
    • AnEmailIsSentAfterACoffeeSale
    • 2 other tests you think are missing.



##Running the code

1.  Install the composer 
`composer install`
2.  Setup your env file
3.  Create database
4.  Migrate database
`php artisan migrate`
5.  To run the testcases 
`vendor/bin/phpunit`