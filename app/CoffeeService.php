<?php

namespace App;

class CoffeeService {
	protected $saleProducts;
	protected $price;
	function __construct() {
		$this->saleProducts = collect();
	}

	public function recordSale($saleProducts) {
		$this->saleProducts->push($saleProducts);
		$this->computePrice();
		// Mail::to('ahsansalaldaha@gmail.com')->send(
		// 	(new SaleNotification($sale))
		// );

	}

	public function computePrice() {
		$price = 0;
		foreach ($this->saleProducts as $sale) {
			$price += $sale->getPrice();
		}
		$this->price = $price;
	}

}
