<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CoffeeServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot() {
		//
	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->bind('coffee-service', 'App\Sale');
		// $this->app->singleton(\App\Sale::class, function ($app) {
		// 	return new \App\Sale();
		// });
	}
}
