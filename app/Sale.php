<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model {
	protected $dates = ['created_at'];

	public function getPriceAttribute() {
		return $this->saleProducts->sum('price');
	}

	public function saleProducts() {
		return $this->hasMany(\App\SaleProduct::class);
	}
	public function products() {
		return $this->belongsToMany(\App\Product::class, 'sale_products');
	}

	public function getSoldAtAttribute() {
		return $this->created_at;
	}

	public function recordSale(Array $coffee_builders) {

		\DB::beginTransaction();

		try {
			$sale = \App\Sale::create();

			foreach ($coffee_builders as $coffee_builder) {
				$product = $coffee_builder->createProduct();
				$sale->products()->attach($product, ['price' => $product->price]);
			}
			\DB::commit();

			// Send email
			$this->sendNotification($sale);

			return $sale;

		} catch (\Exception $e) {
			throw new \Exception($e->getMessage(), 503);
			\DB::rollback();
		}

	}
	// cant add listener as sales has sale products so we can neither implement on sale created event nor on sale product created

	public function sendNotification($sale) {
		\Mail::to('ahsansalaldaha@gmail.com')->queue(
			(new \App\Mail\SaleNotification($sale))
		);
	}

}
