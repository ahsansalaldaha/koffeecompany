<?php

namespace App\Observers;
use \App\Sale;

class SaleObserver {

	// Cant send mail on created event as for sale we need atleast one saleProduct and for sale product we will firstly create sale object and sale will fire up and will have nothing

	public function created(Sale $sale) {

		// defaulting email address to my email address
		// Mail::to('ahsansalaldaha@gmail.com')->send(
		// 	(new SaleNotification($sale))
		// );
	}
}
