<?php
namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class CoffeeService extends Facade {
	protected static function getFacadeAccessor() {
		return 'coffee-service';
	}
}