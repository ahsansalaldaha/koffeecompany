<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	protected $fillable = ['base_ingredient_id', 'name'];
	public function getAddonPrice() {
		return $this->addons->sum('price');
	}

	public function getBaseIngredientPrice() {
		return $this->baseIngredient->price;
	}

	public function getPriceAttribute() {
		return $this->getBaseIngredientPrice() + $this->getAddonPrice();
	}

	public function baseIngredient() {
		return $this->belongsTo(\App\BaseIngredient::class);
	}

	public function addons() {
		return $this->belongsToMany(\App\Addon::class, 'product_addons');
	}

}
