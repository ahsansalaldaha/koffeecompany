<?php

namespace App;

class CoffeeBuilder {

	protected $base_ingredient;
	protected $addons;

	function __construct($baseIngredient = null) {
		$this->addons = collect();
		$this->setBaseIngredient($baseIngredient);
	}

	public function setBaseIngredient($name) {
		$base_ingredient = \App\BaseIngredient::where('name', $name)->first();
		if (!$base_ingredient) {
			throw new \Exception("Ingredient '" . $name . "' not found", 501);
		}
		$this->base_ingredient = $base_ingredient;
	}

	public function add($name) {

		if ($this->addons->count() == 5) {
			throw new \Exception("Cant add more then 5 addons", 504);
		}

		$addon = \App\Addon::where('name', $name)->first();
		if (!$addon) {
			throw new \Exception("Addon '" . $name . "' not found", 502);
		}
		$this->addons[] = $addon;
	}

	public function remove($name) {

		// not implementing error if not found
		$this->addons = $this->addons->reject(function ($addon) use ($name) {
			if ($addon->name == $name) {
				return true;
			}
			return false;
		});

	}

	public function getAddOns() {
		$addons = [];
		foreach ($this->addons as $addon) {
			$addons[] = $addon->name;
		}
		return $addons;
	}

	public function getPrice() {
		return $this->base_ingredient->price + $this->addons->sum('price');
	}

	public function copy() {
		return clone $this;
	}

	public function createProduct() {
		if (!$this->base_ingredient) {
			throw new \Exception("Base Ingredient not found", 501);
		}

		\DB::beginTransaction();

		try {

			$faker = \Faker\Factory::create();
			// Adding Fakar Name
			$product = \App\Product::create([
				'base_ingredient_id' => $this->base_ingredient->id,
				'name' => $faker->name,
			]);
			foreach ($this->addons as $addon) {
				$product->addons()->save($addon);
			}
			\DB::commit();
			return $product;

		} catch (\Exception $e) {
			throw new \Exception("Error creating product", 503);
			\DB::rollback();
		}

	}

}
