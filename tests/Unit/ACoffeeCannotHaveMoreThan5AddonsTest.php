<?php

namespace Tests\Unit;

use Tests\TestCase;

class ACoffeeCannotHaveMoreThan5AddonsTest extends TestCase {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {
		try {
			$coffee1 = new \App\CoffeeBuilder("Large Cup Of Coffee");
			$coffee1->add('chocolate');
			$coffee1->add('vanilla');
			$coffee1->add('vanilla');
			$coffee1->add('vanilla');
			$coffee1->add('vanilla');
			$coffee1->add('whole milk');
			$this->assertTrue(false);

		} catch (\Exception $e) {
			$this->assertTrue(true);

		}

	}
}
