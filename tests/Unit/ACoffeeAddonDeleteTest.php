<?php

namespace Tests\Unit;

use Tests\TestCase;

class ACoffeeAddonDeleteTest extends TestCase {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {
		$coffee1 = new \App\CoffeeBuilder("Large Cup Of Coffee");
		$coffee1->add('chocolate');
		$coffee1->add('vanilla');
		$coffee1->add('vanilla');
		$coffee1->add('vanilla');
		$coffee1->add('whole milk');

		$coffee1->remove('whole milk');
		foreach ($coffee1->getAddOns() as $addon) {
			if ($addon == 'whole milk') {
				$this->assertTrue(false);
			}
		}
		$this->assertTrue(true);
	}
}
