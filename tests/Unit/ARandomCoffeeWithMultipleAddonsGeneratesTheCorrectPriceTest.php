<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ARandomCoffeeWithMultipleAddonsGeneratesTheCorrectPriceTest extends TestCase {
	use WithFaker;
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {
		$addons = factory(App\Addon::class, rand(1, 5));
		$addons_total = 0;
		foreach ($addons as $key => $addon) {
			$addons_total += $addon->price;
		}
		$base_ingredient = factory(\App\BaseIngredient::class)->create();

		$product = new \App\Product();
		$product->name = $this->faker->name;
		$product->base_ingredient_id = $base_ingredient->id;
		$product->save();
		foreach ($addons as $addon) {
			$product->addons()->save($addon);
		}

		$this->assertEquals($product->price, $base_ingredient->price + $addons_total);
	}
}
