<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ACoffeeCannotBeCreatedWithoutABaseIngredientTest extends TestCase {
	use WithFaker;
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {
		try {
			$coffee1 = new \App\CoffeeBuilder();
			$this->assertTrue(false);

		} catch (\Exception $e) {
			$this->assertTrue(true);

		}
	}
}
