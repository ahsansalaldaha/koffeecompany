<?php

namespace Tests\Unit;

use Tests\TestCase;

class ASingleCoffeeSaleGeneratesARecordInTheDatabaseTest extends TestCase {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {

		try {
			$coffee1 = new \App\CoffeeBuilder("Large Cup Of Coffee");
			$coffee1->add('chocolate');
			$coffee1->add('vanilla');
			$coffee1->add('vanilla');
			$coffee1->add('vanilla');
			$coffee1->add('whole milk');
			$sale = app('coffee-service')->recordSale([$coffee1]);
			$this->assertTrue(true);

		} catch (\Exception $e) {
			$this->assertTrue(false);

		}

	}
}
