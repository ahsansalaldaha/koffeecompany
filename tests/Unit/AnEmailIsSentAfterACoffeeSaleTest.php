<?php

namespace Tests\Unit;

use App\Mail\SaleNotification;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AnEmailIsSentAfterACoffeeSaleTest extends TestCase {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {

		$coffee1 = new \App\CoffeeBuilder("Large Cup Of Coffee");
		$coffee1->add('chocolate');
		$coffee1->add('vanilla');
		$coffee1->add('whole milk');
		$coffee1->add('skim milk');

		Mail::fake();
		$sale = app('coffee-service')->recordSale([$coffee1]);
		Mail::assertQueued(SaleNotification::class);
	}
}
