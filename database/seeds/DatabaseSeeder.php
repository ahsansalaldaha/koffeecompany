<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {

		\App\BaseIngredient::create([
			'name' => 'Large Cup Of Coffee',
			'price' => 2.00,
		]);
		\App\BaseIngredient::create([
			'name' => 'Small Cup Of Coffee',
			'price' => 1.00,
		]);

		\App\Addon::create([
			'name' => 'chocolate',
			'price' => 0.85,
		]);

		\App\Addon::create([
			'name' => 'vanilla',
			'price' => 0.75,
		]);

		\App\Addon::create([
			'name' => 'whole milk',
			'price' => 1.25,
		]);

		\App\Addon::create([
			'name' => 'skim milk',
			'price' => 1.00,
		]);

		$sales = factory(App\Sale::class, 2)->create()->each(function ($sale) {

			$products = factory(App\Product::class, 2)
				->create()
				->each(function ($product) {
					$product->addons()->save(factory(App\Addon::class)->make());
					$product->addons()->save(factory(App\Addon::class)->make());
					$product->addons()->save(factory(App\Addon::class)->make());
					$product->addons()->save(factory(App\Addon::class)->make());
					$product->addons()->save(factory(App\Addon::class)->make());
				});

			foreach ($products as $product) {
				$sale->products()->attach($product, ['price' => $product->price]);
			}
		});

		// $this->call(UsersTableSeeder::class);
	}
}
