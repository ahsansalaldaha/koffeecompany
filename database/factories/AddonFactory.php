<?php

use Faker\Generator as Faker;

$factory->define(App\Addon::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'price' => $faker->randomFloat(2, 0, 100),
	];
});
