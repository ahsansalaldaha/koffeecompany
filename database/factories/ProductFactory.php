<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'base_ingredient_id' => function () {
			return factory(App\BaseIngredient::class)->create()->id;
		},
	];
});
