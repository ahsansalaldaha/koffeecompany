<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleProductsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('sale_products', function (Blueprint $table) {
			$table->increments('id');

			$table->double('price', 8, 2);

			$table->integer('product_id')->unsigned()->nullable();
			$table->foreign('product_id')
				->references('id')->on('products')
				->onDelete('set null');
			$table->index(['product_id']);

			$table->integer('sale_id')->unsigned()->nullable();
			$table->foreign('sale_id')
				->references('id')->on('sales')
				->onDelete('set null');
			$table->index(['sale_id']);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('sale_products');
	}
}
