<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAddonsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('product_addons', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('addon_id')->unsigned()->nullable();
			$table->foreign('addon_id')
				->references('id')->on('addons')
				->onDelete('set null');
			$table->index(['addon_id']);

			$table->integer('product_id')->unsigned()->nullable();
			$table->foreign('product_id')
				->references('id')->on('products')
				->onDelete('cascade');
			$table->index(['product_id']);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('product_addons');
	}
}
